#![allow(unused_variables)]

fn main() {
    ex01();
    ex02();
    ex03();
    ex04();
    ex05();
    ex06();
}

fn ex06() {
    // Fix the error
    let names = [String::from("Sunfei"), "Sunface".to_string()];
    // `Get` returns an Option<T>, it's safe to use
    let name0 = names.get(0).unwrap();
    // But indexing is not safe
    let _name1 = &names[1];
    println!("Success!");
}

fn ex05() {
    let arr = ['a', 'b', 'c'];
    let ele = arr[0]; // Only modify this line to make the code work!
    assert_eq!(ele, 'a');
    println!("Success!");
}

fn ex04() {
    // Fix the error
    let _arr = [1, 2, 3];
    println!("Success!");
}

fn ex03() {
    // Fill the blank
    let list: [i32; 100] = [1; 100];
    assert_eq!(list[0], 1);
    assert_eq!(list.len(), 100);
    println!("Success!");
}

fn ex02() {
    // We can ignore parts of the array type or even the whole type, let the compiler infer it for us
    let arr0 = [1, 2, 3];
    let arr: [_; 3] = ['a', 'b', 'c'];
    // Fill the blank
    // Arrays are stack allocated, `std::mem::size_of_val` returns the bytes which an array occupies
    // A char takes 4 bytes in Rust: Unicode char
    assert_eq!(std::mem::size_of_val(&arr), 12);
    println!("Success!");
}

fn ex01() {
    // Fill the blank with proper array type
    let arr: [i32; 5] = [1, 2, 3, 4, 5];
    // Modify the code below to make it work
    assert_eq!(arr.len(), 5);
    println!("Success!");
}


