#![allow(unused_variables)]

fn main() {
    ex01();
    ex02();
    ex03();
    ex04();
    ex05();
    ex06();
    ex07();
    ex08();
    ex09();
    ex10();
    ex11();
}

fn ex11() {
    // Fill in the blank
    let mut count = 0;
    'outer: loop {
        'inner1: loop {
            if count >= 20 {
                // This would break only the inner1 loop
                break 'inner1; // `break` is also works.
            }
            count += 2;
        }

        count += 5;

        'inner2: loop {
            if count >= 30 {
                // This breaks the outer loop
                break 'outer;
            }

            // This will continue the outer loop
            continue 'outer;
        }
    }
    assert!(count == 30);
    println!("Success!");
}

fn ex10() {
    // Fill in the blank
    let mut counter = 0;
    let result = loop {
        counter += 1;

        if counter == 10 {
            break 20;
        }
    };
    assert_eq!(result, 20);
    println!("Success!");
}

fn ex09() {
    // Fill in the blanks
    let mut count = 0u32;
    println!("Let's count until infinity!");
    // Infinite loop
    loop {
        count += 1;

        if count == 3 {
            println!("three");

            // Skip the rest of this iteration
            continue;
        }

        println!("{}", count);

        if count == 5 {
            println!("OK, that's enough");

            break;
        }
    }
    assert_eq!(count, 5);
    println!("Success!");
}

fn ex08() {
    // Fill in the blanks
    let mut n = 0;
    for i in 0..=100 {
        if n != 66 {
            n += 1;
            continue;
        }
        break;
    }
    assert_eq!(n, 66);
    println!("Success!");
}

fn ex07() {
    // Fill in the blank
    let mut n = 0;
    for i in 0..=100 {
        if n == 66 {
            break;
        }
        n += 1;
    }
    assert_eq!(n, 66);
    println!("Success!");
}

fn ex06() {
// Fill in the blanks to make the last println! work !
    // A counter variable
    let mut n = 1;
    // Loop while the condition is true
    while n < 20 {
        if n % 15 == 0 {
            println!("fizzbuzz");
        } else if n % 3 == 0 {
            println!("fizz");
        } else if n % 5 == 0 {
            println!("buzz");
        } else {
            println!("{}", n);
        }
        n += 1;
    }
    println!("n reached {}, so loop is over", n);
}

fn ex05() {
    let a = [4, 3, 2, 1];
    // Iterate the indexing and value in 'a'
    for (i, v) in a.iter().enumerate() {
        println!("The {}th element is {}", i + 1, v);
    }
}

fn ex04() {
    // Fix the errors without adding or removing lines
    let names = [String::from("liming"), String::from("hanmeimei")];
    for name in &names {
        // Do something with name...
    }
    println!("{:?}", names);
    let numbers = [1, 2, 3];
    // The elements in numbers are Copy，so there is no move here
    for n in &numbers {
        // Do something with n...
    }
    println!("{:?}", numbers);
}

fn ex03() {
    for n in 1..100 { // modify this line to make the code work
        if n == 100 {
            panic!("NEVER LET THIS RUN")
        }
    }
    println!("Success!");
}

fn ex02() {
    // Fix the errors
    let n = 5;
    let big_n =
        if n < 10 && n > -10 {
            println!(", and is a small number, increase ten-fold");

            10 * n
        } else {
            println!(", and is a big number, halve the number");

            n / 2.0 as i32
        };
    println!("{} -> {}", n, big_n);
}

fn ex01() {
    // Fill in the blanks
    let n = 5;
    if n < 0 {
        println!("{} is negative", n);
    } else if n > 0 {
        println!("{} is positive", n);
    } else {
        println!("{} is zero", n);
    }
}
