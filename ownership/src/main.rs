fn main() {
    ex01();
    ex02();
    ex03();
    ex04();
    ex05();
    ex06();
    ex07();
    partial_move_ex08();
    ex_09();
}

fn ex_09() {
    let t = (String::from("hello"), String::from("world"));
    // Fill the blanks
    let (s1,s2) = t.to_owned();
    println!("{:?}, {:?}, {:?}", s1, s2, t); // -> "hello", "world", ("hello", "world")
}

fn partial_move_ex08() {
    #[derive(Debug)]
    struct Person {
        name: String,
        age: Box<u8>,
    }

    let person = Person {
        name: String::from("Alice"),
        age: Box::new(20),
    };

    // `name` is moved out of person, but `age` is referenced
    let Person { name, ref age } = person;

    println!("The person's age is {}", age);

    println!("The person's name is {}", name);

    // Error! borrow of partially moved value: `person` partial move occurs
    //println!("The person struct is {:?}", person);

    // `person` cannot be used but `person.age` can be used as it is not moved
    println!("The person's age from person struct is {}", person.age);


    let t = (String::from("hello"), String::from("world"));
    let _s = t.0;
    // Modify this line only, don't use `_s`
    println!("{:?}", t.1);
}

fn ex07() {
    let x = Box::new(5);
    let mut y = Box::new(0);      // update this line, don't change other lines!
    *y = 4;
    assert_eq!(*x, 5);
    println!("Success!");
}

fn ex06() {
    // make the necessary variable mutable
    let s = String::from("Hello ");
    let mut s1 = s;
    s1.push_str("World!");
    println!("Success!");
}

fn ex05() {
    // Don't use clone, use copy instead
    let x = (1, 2, (), "hello".to_string());
    let y = &x;
    println!("{:?}, {:?}", x, y);
}

fn ex04() {
    // Fix the error without removing any code
    let s = String::from("Hello World");
    print_str(s.to_owned());
    println!("{}", s);

    fn print_str(s: String) {
        println!("{}", s)
    }
}

fn ex03() {
    let s = give_ownership();
    println!("{}", s);

    // Only modify the code below!
    fn give_ownership() -> String {
        let s = String::from("Hello world");
        s
    }
}

fn ex02() {
    // Don't modify code in main!
    let s1 = String::from("Hello world");
    let s2 = take_ownership(s1);

    println!("{}", s2);

    // Only modify the code below!
    fn take_ownership(s: String) -> String {
        println!("{}", s);
        s
    }
}

fn ex01() {

    // Use as many approaches as you can to make it work
    let x = &String::from("Hello world");
    let y = x;
    println!("{}, {}", x, y);

    let x = String::from("Hello world");
    let y = x.to_owned();
    println!("{}, {}", x, y);

    let x = String::from("Hello world");
    let y = x.as_str();
    println!("{}, {}", x, y);

    let x = "Hello world";
    let y = x;
    println!("{}, {}", x, y);
}
