#![allow(dead_code, unused_variables)]

use std::io::Error;
use std::num::ParseIntError;

fn main() {
    ex01();
    ex02();
    ex03();
    ex04();
    ex05();
}

fn ex05() {
    #[derive(Debug, PartialEq)]
    struct EvenNum(i32);
    impl TryFrom<i32> for EvenNum {
        type Error = ();
        // IMPLEMENT `try_from`
        fn try_from(value: i32) -> Result<Self, Self::Error> {
            if value % 2 == 0 {
                Ok(EvenNum(value))
            } else {
                Err(())
            }
        }
    }
    assert_eq!(EvenNum::try_from(8), Ok(EvenNum(8)));
    assert_eq!(EvenNum::try_from(5), Err(()));
    // FILL in the blanks
    let result: Result<EvenNum, ()> = 8i32.try_into();
    assert_eq!(result, Ok(EvenNum(8)));
    let result: Result<EvenNum, ()> = 5i32.try_into();
    assert_eq!(result, Err(()));
    println!("Success! 05");
}

fn ex04() {
    // TryFrom and TryInto are included in `std::prelude`, so there is no need to introduce it into the current scope
// use std::convert::TryInto;
    let n: i16 = 256;
    // Into trait has a method `into`,
    // hence TryInto has a method ?
    let n: u8 = match n.try_into() {
        Ok(n) => n,
        Err(e) => {
            println!("there is an error when converting: {:?}, but we catch it", e.to_string());
            0
        }
    };
    assert_eq!(n, 0);
    println!("Success! 04");
}

fn ex03() {
    use std::fs;
    use std::io;
    use std::num;

    enum CliError {
        IoError(io::Error),
        ParseError(num::ParseIntError),
    }

    impl From<io::Error> for CliError {
        // IMPLEMENT from method
        fn from(value: Error) -> Self {
            CliError::IoError(value)
        }
    }

    impl From<num::ParseIntError> for CliError {
        // IMPLEMENT from method
        fn from(value: ParseIntError) -> Self {
            CliError::ParseError(value)
        }
    }

    fn open_and_parse_file(file_name: &str) -> Result<i32, CliError> {
        // ? automatically converts io::Error to CliError
        let contents = fs::read_to_string(&file_name)?;
        // num::ParseIntError -> CliError
        let num: i32 = contents.trim().parse()?;
        Ok(num)
    }
    println!("Success! 03");
}

fn ex02() {
    // From is now included in `std::prelude`, so there is no need to introduce it into the current scope
// use std::convert::From;
    #[derive(Debug)]
    struct Number {
        value: i32,
    }
    impl From<i32> for Number {
        // IMPLEMENT `from` method
        fn from(value: i32) -> Self {
            Number { value }
        }
    }
    // FILL in the blanks
    let num = Number::from(30);
    assert_eq!(num.value, 30);
    let num: Number = 30.into();
    assert_eq!(num.value, 30);
    println!("Success! 02");
}

fn ex01() {
    // impl From<bool> for i32
    let i1: i32 = false.into();
    let i2: i32 = i32::from(false);
    assert_eq!(i1, i2);
    assert_eq!(i1, 0);
    // FIX the error in two ways
    /* 1. use a similar type which `impl From<char>`, maybe you
    should check the docs mentioned above to find the answer */
    // 2. a keyword from the last chapter
    let i3: i32 = ('a' as u8).into();
    // FIX the error in two ways
    let s: String = String::from('a');
    let s2: String = 'a'.into();
    println!("Success! 01");
}
