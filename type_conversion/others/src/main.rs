use std::fmt::Formatter;

fn main() {
    ex01();
    ex02();
    ex03();
}

fn ex01() {
    //     Convert any type to String
//     To convert any type to String, you can simply use the ToString trait
//     for that type. Rather than doing that directly, you should implement
//     the fmt::Display trait which will automatically provides ToString
//     and also allows you to print the type with println!.
    use std::fmt;

    struct Point {
        x: i32,
        y: i32,
    }

    impl fmt::Display for Point {
        // IMPLEMENT fmt method
        fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
            write!(f, "The point is ({}, {})", self.x, self.y)
        }
    }

    let origin = Point { x: 0, y: 0 };
    // FILL in the blanks
    assert_eq!(origin.to_string(), "The point is (0, 0)");
    assert_eq!(format!("{}", origin), "The point is (0, 0)");
    println!("Success! 01");
}

fn ex02() {
    //   Parse a String
//   🌟🌟🌟 We can use parse method to convert a String into a i32 number,
//   this is because FromStr is implemented for i32 type in standard library:
//   impl FromStr for i32
// To use `from_str` method, you need to introduce this trait into the current scope.
    use std::str::FromStr;
    let parsed: i32 = "5".parse().unwrap();
    let turbo_parsed = "10".parse::<i32>().unwrap();
    let from_str = i32::from_str("20").unwrap();
    let sum = parsed + turbo_parsed + from_str;
    assert_eq!(sum, 35);
    println!("Success! 02");
}

fn ex03() {
    //    🌟🌟 We can also implement the FromStr trait for our custom types
    use std::str::FromStr;
    use std::num::ParseIntError;

    #[derive(Debug, PartialEq)]
    struct Point {
        x: i32,
        y: i32,
    }

    impl FromStr for Point {
        type Err = ParseIntError;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let coords: Vec<&str> = s.trim_matches(|p| p == '(' || p == ')')
                .split(',')
                .map(|x| x.trim())
                .collect();

            let x_fromstr = coords[0].parse::<i32>()?;
            let y_fromstr = coords[1].parse::<i32>()?;

            Ok(Point { x: x_fromstr, y: y_fromstr })
        }
    }
    // FILL in the blanks in two ways
    // DON'T change code anywhere else
    let p = Point::from_str("(3,4)");
    assert_eq!(p.unwrap(), Point { x: 3, y: 4 });
    println!("Success! 03");
}