fn main() {
    ex01();
    ex02();
    ex03();
    ex04();
    ex05();
    ex06();
}

fn ex06() {
    //Type alias
    // Using std::result::Result<T, ParseIntError> everywhere is verbose and tedious,
    // we can use alias for this purpose.
    //
    // At a module level, creating aliases can be particularly helpful.
    // Errors found in a specific module often has the same Err type, so a single alias
    // can succinctly defined all associated Results. This is so useful even the
    // std library supplies one: io::Result.
    use std::num::ParseIntError;

    // FILL in the blank
    type Res<T> = std::result::Result<T, ParseIntError>;

    // Use the above alias to refer to our specific `Result` type.
    fn multiply(first_number_str: &str, second_number_str: &str) -> Res<i32> {
        first_number_str.parse::<i32>().and_then(|first_number| {
            second_number_str.parse::<i32>().map(|second_number| first_number * second_number)
        })
    }

    // Here, the alias again allows us to save some space.
    fn print(result: Res<i32>) {
        match result {
            Ok(n) => println!("n is {}", n),
            Err(e) => println!("Error: {}", e),
        }
    }

    print(multiply("10", "2"));
    print(multiply("t", "2"));
    println!("Success! 06");
}

fn ex05() {
    use std::num::ParseIntError;

    // With the return type rewritten, we use pattern matching without `unwrap()`.
// But it's so Verbose...
    fn multiply(n1_str: &str, n2_str: &str) -> Result<i32, ParseIntError> {
        match n1_str.parse::<i32>() {
            Ok(n1) => {
                match n2_str.parse::<i32>() {
                    Ok(n2) => {
                        Ok(n1 * n2)
                    }
                    Err(e) => Err(e),
                }
            }
            Err(e) => Err(e),
        }
    }

    // Rewriting `multiply` to make it succinct
// You should use BOTH of  `and_then` and `map` here.
    fn multiply1(n1_str: &str, n2_str: &str) -> Result<i32, ParseIntError> {
        // IMPLEMENT...
        n1_str.parse::<i32>()
            .and_then(|n1| n2_str.parse::<i32>()
                .map(|n2| n1 * n2))
    }

    fn print(result: Result<i32, ParseIntError>) {
        match result {
            Ok(n) => println!("n is {}", n),
            Err(e) => println!("Error: {}", e),
        }
    }

    // This still presents a reasonable answer.
    let twenty = multiply1("10", "2");
    print(twenty);
    // The following now provides a much more helpful error message.
    let tt = multiply("t", "2");
    print(tt);
    println!("Success! 05");
}

fn ex04() {
    // map & and_then
    // map and and_then are two common combinators for Result<T, E> (also for Option<T>).
    use std::num::ParseIntError;

    // FILL in the blank in two ways: map, and then
    fn add_two(n_str: &str) -> Result<i32, ParseIntError> {
        n_str.parse::<i32>().map(|n| n + 2)
    }

    fn add_two1(n_str: &str) -> Result<i32, ParseIntError> {
        n_str.parse::<i32>().and_then(|n| Ok(n + 2))
    }

    assert_eq!(add_two("4").unwrap(), 6);
    assert_eq!(add_two1("4").unwrap(), 6);
    println!("Success! 04");
}

fn ex03() {
    use std::fs::File;
    use std::io::{self, Read};

    fn read_file1() -> Result<String, io::Error> {
        let f = File::open("hello.txt");
        let mut f = match f {
            Ok(file) => file,
            Err(e) => return Err(e),
        };

        let mut s = String::new();
        match f.read_to_string(&mut s) {
            Ok(_) => Ok(s),
            Err(e) => Err(e),
        }
    }

    // FILL in the blanks with one code line
// DON'T change any code lines
    fn read_file2() -> Result<String, io::Error> {
        let mut s = String::new();
        File::open("hello.txt")?.read_to_string(&mut s)?;
        Ok(s)
    }

    assert_eq!(read_file1().unwrap_err().to_string(), read_file2().unwrap_err().to_string());
    println!("Success! 03");
}

fn ex02() {
//    ? is almost exactly equivalent to unwrap, but ? returns instead of panic on Err.

    use std::num::ParseIntError;

    // IMPLEMENT multiply with ?
// DON'T use unwrap here
    fn multiply(n1_str: &str, n2_str: &str) -> Result<i32, ParseIntError> {
        Ok(n1_str.parse::<i32>()? * n2_str.parse::<i32>()?)
    }
    assert_eq!(multiply("3", "4").unwrap(), 12);
    println!("Success! 02");
}

fn ex01() {
    // FILL in the blanks and FIX the errors
    use std::num::ParseIntError;

    fn multiply(n1_str: &str, n2_str: &str) -> Result<i32, ParseIntError> {
        let n1 = n1_str.parse::<i32>();
        let n2 = n2_str.parse::<i32>();
        Ok(n1.unwrap() * n2.unwrap())
    }

    let result = multiply("10", "2");
    assert_eq!(result, Ok(20));

    let result = multiply("4", "2");
    assert_eq!(result.unwrap(), 8);

    println!("Success! 01");
}
