#![allow(unused_variables, dead_code)]

fn main() {
    // ex01();
    ex02();
}

fn ex02() {
    // MAKE the code work by fixing all panics
    assert_eq!("abc".as_bytes(), [97, 98, 99]);
    let v = vec![1, 2, 3, 10];
    let ele = v[3];
    // unwrap may panic when get return a None
    let ele = v.get(3).unwrap();

    // Sometimes, the compiler is unable to find the overflow errors for you in compile time ,so a panic will occur
    let v = production_rate_per_hour(2);

    // because of the same reason as above, we have to wrap it in a function to make the panic occur
    divide(15, 0);
    println!("Success! 02");
    fn divide(x: u8, y: u8) {
        match y {
            0 => println!("Divide by zero"),
            _ => println!("{}", x / y)
        }
    }
    fn production_rate_per_hour(speed: u32) -> f64 {
        let cph: u32 = 221;
        match speed {
            1..=4 => (speed * cph) as f64,
            5..=8 => (speed * cph) as f64 * 0.9,
            9..=10 => (speed * cph) as f64 * 0.77,
            _ => 0 as f64,
        }
    }

    pub fn working_items_per_minute(speed: u32) -> u32 {
        (production_rate_per_hour(speed) / 60 as f64) as u32
    }
}

fn ex01() {
    fn drink(beverage: &str) {
        if beverage == "lemonade" {
            println!("Success!");
            // IMPLEMENT the below code
            panic!("no lemonade allowed")
        }
        println!("Exercise Failed if printing out this line!");
    }
    drink("lemonade");
    println!("Exercise Failed if printing out this line!");
}
