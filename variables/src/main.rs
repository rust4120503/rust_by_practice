#![allow(unused_variables, unused_assignments)]

fn main() {
    binding_and_mutability();
    scope();
    shadowing();
    unused_variables();
    destructuring();
}

fn destructuring() {
    // Fix the error below with least amount of modification
    let (mut x, y) = (1, 2);
    x += 2;
    assert_eq!(x, 3);
    assert_eq!(y, 2);

    println!("Success!");


    let (x, y);
    (x, ..) = (3, 4);
    [.., y] = [1, 2];
    // Fill the blank to make the code work
    assert_eq!([x, y], [3,2]);
    println!("Success!");
}

fn unused_variables() {
    #[allow(unused_variables)]
        let x = 1;
// Warning: unused variable: `x`
}

fn shadowing() {
    // Only modify `assert_eq!` to make the `println!` work(print `42` in terminal)
    let x: i32 = 5;
    {
        let x = 12;
        assert_eq!(x, 12);
    }
    assert_eq!(x, 5);

    let x = 42;
    println!("{}", x); // Prints "42".


    // Remove a line in the code to make it compile
    let mut x: i32 = 1;
    x = 7;
    // Shadowing and re-binding
    x += 3;

    let y = 4;
    // Shadowing
    let y = "I can also be bound to text!";
    println!("Success!");
}

fn binding_and_mutability() {
    // Fix the error below with least amount of modification to the code
    let x: i32 = 5; // Uninitialized but used, ERROR !
    let y: i32; // Uninitialized but also unused, only a Warning !
    assert_eq!(x, 5);
    println!("Success!");

    // Fill the blanks in the code to make it compile
    let mut x = 1;
    x += 2;
    assert_eq!(x, 3);
    println!("Success!");
}

fn scope() {

    // Fix the error below with least amount of modification
    let x: i32 = 10;
    let y: i32;
    {
        y = 5;
        println!("The value of x is {} and value of y is {}", x, y);
    }
    println!("The value of x is {} and value of y is {}", x, y);


    // Fix the error with the use of define_x
    println!("{}, world", define_x());

    fn define_x() -> String {
        let x = "hello";
        x.to_string()
    }
}