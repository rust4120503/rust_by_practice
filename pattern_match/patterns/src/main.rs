fn main() {
    ex01();
    ex02();
    ex03();
    ex04();
    ex05();
    ex06();
}

fn ex06() {
    // FIX the error with least changing
// DON'T remove any code line
    let mut v = String::from("hello,");
    let r = &mut v;
    let r = match r {
        // type of value is &mut String
        value => {
            value.push_str(" world!");
            value
        }
    };
    println!("{}", r);
}

fn ex05() {
    // Fill the blank to make the code work
    let numbers = (2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048);
    match numbers {
        (first, .., last) => {
            assert_eq!(first, 2);
            assert_eq!(last, 2048);
        }
    }
    println!("Success!");
}

fn ex04() {
    // Fill in the blank to make the code work, `split` MUST be used
    let num = Some(4);
    let split = 5;
    match num {
        Some(x) if x < split => assert!(x < split),
        Some(x) => assert!(x >= split),
        None => (),
    }
    println!("Success!");
}

fn ex03() {
    // Fix the errors
    enum Message {
        Hello { id: i32 },
    }

    let msg = Message::Hello { id: 5 };
    match msg {
        Message::Hello {
            id: id @ (3..=7),
        } => println!("Found an id in range [3, 7]: {}", id),
        Message::Hello { id: newid @ (10 | 11 | 12) } => {
            println!("Found an id in another range [10, 12]: {}", newid)
        }
        Message::Hello { id } => println!("Found some other id: {}", id),
    }
}

fn ex02() {
    struct Point {
        x: i32,
        y: i32,
    }

    // Fill in the blank to let p match the second arm
    let p = Point { x: 5, y: 30 };
    match p {
        Point { x, y: 0 } => println!("On the x axis at {}", x),
        // Second arm
        Point { x: 0..=5, y: y @ (10 | 20 | 30) } => println!("On the y axis at {}", y),
        Point { x, y } => println!("On neither axis: ({}, {})", x, y),
    }
}

fn ex01() {
    match_number(4);
    match_number(0);
    match_number(-100);

    fn match_number(n: i32) {
        match n {
            // Match a single value
            1 => println!("One!"),
            // Fill in the blank with `|`, DON'T use `..` or `..=`
            2 | 3 | 4 | 5 => println!("match 2 -> 5"),
            // Match an inclusive range
            6..=10 => {
                println!("match 6 -> 10")
            }
            _ => {
                println!("match -infinite -> 0 or 11 -> +infinite")
            }
        }
    }
}
